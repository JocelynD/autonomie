/*
 * Module name : ProductComponent
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import ProductCollectionView from './ProductCollectionView.js';

const template = require('./templates/ProductComponent.mustache');

const ProductComponent = Mn.View.extend({
    template: template,
    className: '',
    regions: {
        collection: '.collection'
    },
    // Listen to child view events
    childViewEvents: {
        "product:up": "onModelOrderUp",
        "product:down": "onModelOrderDown",
        "product:edit": "onProductEdit",
        'product:delete': "onProductDelete",
        "product:duplicate": "onProductDuplicate",
    },
    initialize(){
        this.app = Radio.channel('app');
    },
    onRender(){
        this.showChildView(
            'collection',
            new ProductCollectionView({collection: this.collection})
        );
    },
    onModelOrderUp: function(childView){
        console.log("Moving up the element");
        this.collection.moveUp(childView.model);
    },
    onModelOrderDown: function(childView){
        this.collection.moveDown(childView.model);
    },
    onProductEdit(childView){
        let route = '/products/';
        if (childView.model.get('type_') == 'price_study_work'){
            route = '/works/';
        }
        this.app.trigger('navigate', route + childView.model.get('id'));
    },
    onProductDuplicate(childView){
        console.log("Triggering App.product:duplicate")
        this.app.trigger('product:duplicate', childView);
    },
    onProductDelete(childView){
        this.app.trigger('product:delete', childView);
    },
});
export default ProductComponent
