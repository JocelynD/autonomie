/*
 * File Name :  WorkItemModel
 */
import BaseModel from '../../base/models/BaseModel.js';
import Radio from 'backbone.radio';
import { strToFloat, getTvaPart, round } from '../../math.js';
import { formatAmount } from '../../math.js';

const WorkItemModel = BaseModel.extend({
    props: [
        "id",
        "type_",
        "label",
        "description",
        "ht",
        "ht_editable",
        "supplier_ht",
        "supplier_ht_editable",
        "quantity",
        "unity",
        "unity_editable",
        "tva_id",
        "tva_id_editable",
        "total_ht",
        "product_id",
        "product_id_editable",
        "general_overhead",
        "general_overhead_editable",
        "margin_rate",
        "margin_rate_editable",
        'base_sale_product_id',
        "sync_catalog",
        'locked'
    ],
    inherited_props: [
        'margin_rate',
        'general_overhead',
        'tva_id',
        'product_id'
    ],
    defaults() {
        return {
            quantity: 1,
            locked: true
        }
    },
    validation: {
        'label': {required: true, msg: "Veuillez saisir un nom"},
        description: {required: true, msg: "Veuillez saisir une description"},
        type_: {required: true, msg: "Veuillez choisir un type de produit"}
    },
    icons: {
        sale_product_product: 'box',
        sale_product_material: 'box',
        sale_product_composite: 'product-composite',
        sale_product_training: 'chalkboard-teacher',
        sale_product_work_force: "user",
        sale_product_service_delivery: "hands-helping",
    },
    initialize: function(){
        BaseModel.__super__.initialize.apply(this, arguments);
        this.facade = Radio.channel('facade');
        this.config = Radio.channel('config');
        this.tva_options = this.config.request(
            'get:options',
            'tvas'
        );
        this.product_options = this.config.request(
            'get:options',
            'products'
        );
    },
    supplier_ht_label(){
        return formatAmount(this.get('supplier_ht'), false, false);
    },
    ht_label(){
        return formatAmount(this.get('ht'), false, false);
    },
    tva_label(){
        return this.findLabelFromId('tva_id', 'label', this.tva_options);
    },
    product_label(){
        return this.findLabelFromId('product_id', 'label', this.product_options);
    },
    total_ht_label(){
        return formatAmount(this.get('total_ht'), false, false);
    },
    getIcon(){
        if (_.has(this.icons, this.get('type_'))){
            return this.icons[this.get('type_')];
        } else {
            return false;
        }
    },
    loadFromCatalog(model){
        let datas = model.toJSON();
        datas['base_sale_product_id'] = datas['id'];
        datas['locked'] = true;
        delete datas['id'];
        this.set(datas);
    },
    isFromParent(attribute){
        /*
        * Check if the given attribute's value comes from the parent ProductWork
        */
        if (this.get(attribute + '_editable')){
            return false;
        }
        if (! this.inherited_props.includes(attribute)){
            return false;
        }
        if (! this.collection){
            return false;
        }
        let parent = this.collection._parent;
        if (! parent){
            throw "this._parent has not been set on the WorkItem model's collection we can't answer if it's from parent !!";
        }
        let result = false;
        if (Boolean(parent.get(attribute))){
            result = true;
        }
        return result;
    },
    isFromCatalog(attribute){
        /*
         * Check if the given attribute comes from the catalog
         * Not editable
         */
        let result = false;
        if (this.get('locked')){
            let editable_key = attribute + '_editable';
            if (this.has(editable_key))
            if (! this.get(editable_key) && ! this.isFromParent(attribute)){
                result = true;
            }
        }
        return result;
    }
});
export default WorkItemModel;
