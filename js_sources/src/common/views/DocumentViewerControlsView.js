import Mn from 'backbone.marionette';

const template = require('./templates/DocumentViewerControlsView.mustache');

const DocumentViewerControlsView = Mn.View.extend({
    ui: {
        zoom_in_btn: '.zoom_in',
        zoom_out_btn: '.zoom_out',
        go_previous_btn: '.go_previous',
        go_next_btn: '.go_next',
        open_full_btn: '.open_full',
    },
    template: template,

    triggers: {
        'click @ui.zoom_in_btn': 'zoom:in',
        'click @ui.zoom_out_btn': 'zoom:out',
        'click @ui.go_previous_btn': 'page:previous',
        'click @ui.go_next_btn': 'page:next',
        'click @ui.open_full_btn': 'open',
    },

    templateContext(){
        return {
            multipage: this.getOption('multipage'),
        };
    }
});

export default DocumentViewerControlsView;
