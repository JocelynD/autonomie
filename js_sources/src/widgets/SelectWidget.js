import _ from 'underscore';
import BaseFormWidget from './BaseFormWidget.js';
import { getOpt } from "../tools.js";
import { updateSelectOptions } from '../tools.js';

var template = require('./templates/SelectWidget.mustache');

const SelectWidget = BaseFormWidget.extend({
    /*
     * A select widget
     *
     * Features :
     *  - single or multiple item selector
     *  - optionaly supports empty value
     *
     * Triggers on value selection (two different events to allow two way of event bindings):
     *  - "finish", field_name, field_value 
     *  - "finish:field_name", field_value
     *
     * Also triggers a convenience event to broadcast the name of newly seleceted option
     *  - "labelChange", field_name, field_value
     *
     * Support following options
     *
     *   :param str title: The field label
     *   :param str description:
     *   :param str field_name:
     *   :param list options: The options to render list of objects
     *   :param value: The value to select
     *   :param str id_key: The key of the options used as option's "value", default is "value"
     *   :param str label_key: The key of the options used as label default is label
     *   :param bool editable:
     *   :param obj placeholder: placeholder text, will add an <option> with
     *     value="" ; can be set to empty string for empty label <option>
     *     as placeholder.
     *   :param bool: multiple: does this widget allow selecting multiple targets
     */
    tagName: 'div',
    className: 'form-group',
    template: template,
    ui: {
        select: 'select'
    },
    events: {
        'change @ui.select': "onChange"
    },
    onChange: function(event){
        const select = this.getUI('select');
        const field_value = select.val();

        this.options.value = this.getCurrentValues();
        this.triggerFinish(field_value);
        // specific to select
        this.triggerMethod(
            'labelChange',
            this.getOption('field_name'),
            select.find('option:selected').text(),
        );
    },
    hasVoid(options){
        return !_.isUndefined(
            _.find(
                options,
                function(option){return option.label == '';}
            )
        );
    },
    /** Return currently selected values
     *
     * this.getOption('value') is not always relevant as it contains only the
     * initial value (from widget initialization).
     */
    getCurrentValues: function(){
        let optionNodes = $.makeArray(this.$el.find('option:selected'));
        return optionNodes.map((x) => x.getAttribute('value'));
    },
    initializePlaceHolder: function(placeholder, options) {
        let placeholderOption = {
            [this.id_key]: '',
            [this.label_key]: placeholder,
        };
        if ((!_.findWhere(options, placeholderOption))){
            options.unshift(placeholderOption);
        }
    },
    templateContext: function(){
        let ctx = this.getCommonContext();

        this.id_key = getOpt(this, 'id_key', 'value');
        this.label_key = getOpt(this, 'label_key', 'label');
        // If some ad-hoc filtering of displayed options is required, read
        // CheckBoxListWidget implementation for inspiration.

        // do not use getOption as there is name collision on "options"
        var options = this.options.options || [];

        var initialValue = this.getOption('value');

        let hasInitialValue = ! _.isUndefined(initialValue) && ! _.isNull(initialValue);
        if (hasInitialValue){
            updateSelectOptions(options, initialValue, this.id_key);
        }

        let placeholder = getOpt(this, 'placeholder', undefined);
        let hasPlaceHolder =  ! _.isUndefined(placeholder);
        if (hasPlaceHolder) {
            this.initializePlaceHolder(placeholder, options);
        }
        let multiple = getOpt(this, 'multiple', false);
        let more_ctx =  {
            options:options,
            id_key: this.id_key,
            label_key: this.label_key,
            multiple: multiple,
        };
        return Object.assign(ctx, more_ctx);
    },
});
export default SelectWidget;
