import OrderableCollection from "../../../base/models/OrderableCollection.js";
import TaskGroupModel from './TaskGroupModel.js';
import { ajax_call } from '../../../tools.js';
import Radio from 'backbone.radio';


const TaskGroupCollection = OrderableCollection.extend({
    model: TaskGroupModel,
    initialize: function(options) {
        TaskGroupCollection.__super__.initialize.apply(this, options);
        this.on('saved', this.channelCall);
    },
    channelCall: function(){
        var channel = Radio.channel('facade');
        channel.trigger('changed:task');
    },
    ht: function(){
        var result = 0;
        this.each(function(model){
            result += model.ht();
        });
        return result;
    },
    tvaParts: function(){
        var result = {};
        this.each(function(model){
            var tva_parts = model.tvaParts();
            _.each(tva_parts, function(value, key){
                if (key in result){
                    value += result[key];
                }
                result[key] = value;
            });
        });
        return result;
    },
    ttc: function(){
        var result = 0;
        this.each(function(model){
            result += model.ttc();
        });
        return result;
    },
    validate: function(){
        return true;
    }
});
export default TaskGroupCollection;
