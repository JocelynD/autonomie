import BaseModel from '../../base/models/BaseModel'
import {
    strToFloat
} from '../../math';

const PaymentOptionsModel = BaseModel.extend({
    // Define the props that should be set on your model
    props: [
        'id',
        "deposit",
        'paymentDisplay',
        'payment_times',
        'deposit_amount_ttc',
    ],
    isAmountEditable() {
        return strToFloat(this.get('payment_times')) === -1;
    },
    isDateEditable() {
        return this.get('paymentDisplay') !== 'ALL_NO_DATE';
    }
})
export default PaymentOptionsModel;