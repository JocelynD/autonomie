import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import Validation from 'backbone-validation';

import { formatAmount } from '../../../../math.js';
import {displayServerSuccess, displayServerError} from '../../../../backbone-tools.js';

import TaskLineCollectionView from './TaskLineCollectionView.js';
import TaskLineFormView from './TaskLineFormView.js';
import TaskGroupTotalView from '../TaskGroupTotalView.js';
import PercentDisplayWidget from './PercentDisplayWidget';

const template = require('./templates/TaskGroupView.mustache');

const TaskGroupView = Mn.View.extend({
    tagName: 'div',
    className: 'taskline-group border_left_block content_double_padding row',
    template: template,
    regions: {
        errors: '.errors',
        lines: {el: '.lines', replaceElement: true},
        modalRegion: ".modalregion",
        subtotal: {el: '.subtotal', replaceElement: true},
        slider_container: ".group-slider-container",
    },
    ui: {
        edit_button: 'button.groupedit',
    },
    triggers: {
        'click @ui.edit_button': 'edit',
    },
    childViewEvents: {
        'line:edit': 'onLineEdit',
        'destroy:modal': 'render',
        'slider:clicked': 'onSliderClicked'
    },
    modelEvents: {
        "validated:invalid": "showErrors",
        "validated:valid": "hideErrors",
    },
    initialize: function(options){
        // Collection of task lines
        this.collection = this.model.lines;
        this.listenTo(this.collection, 'sync', this.showLines.bind(this));

        var channel = Radio.channel('facade');
        var configChannel = Radio.channel('config');
        this.listenTo(channel, 'bind:validation', this.bindValidation);
        this.listenTo(channel, 'unbind:validation', this.unbindValidation);
    },
    isEmpty: function(){
        return this.model.lines.length === 0;
    },
    showErrors(model, errors){
        this.$el.addClass('error');
    },
    hideErrors(model){
        this.$el.removeClass('error');
    },
    bindValidation(){
        Validation.bind(this);
    },
    unbindValidation(){
        Validation.unbind(this);
    },
    showLines(){
        /*
         * Show lines if it's not done yet
         */
        if (!_.isNull(this.getChildView('lines'))){
            this.showChildView(
                'lines',
                new TaskLineCollectionView({collection: this.collection})
            );
        }
    },
    onRender: function(){
        if (! this.isEmpty()){
            this.showLines();
        }
        let view;
        view = new TaskGroupTotalView({
            collection: this.collection,
            colspan: 4,
        });
        this.showChildView('subtotal', view);

        if (this.percentWidgetVisible()){
            view = new PercentDisplayWidget({
                'left': this.model.get('current_percent_left'),
                'current': this.model.get('current_percent'),
                'done': 100 - this.model.get('percent_left'),
            });
            this.showChildView('slider_container', view);
        }
    },
    onLineEdit: function(childView){
        this.showTaskLineForm(childView.model, "Modifier le produit", true);
    },
    onSliderClicked(){
        if (this.model.isEditable()){
            this.trigger('edit', this);
        }
    },
    showTaskLineForm: function(model, title, edit){
        var form = new TaskLineFormView(
            {
                model: model,
                title: title,
                destCollection: this.collection,
                edit: edit
            });
        this.showChildView('modalRegion', form);
    },
    onChildviewDestroyModal: function() {
    	this.getRegion('modalRegion').empty();
  	},
    percentWidgetVisible(){
        const show_percent_widget = this.model.hasCurrentPercent();
        return show_percent_widget;
    },
    templateContext: function(){
        const show_edit_button = this.model.isEditable() && ! this.model.hasCurrentPercent();
        const result = {
            ht_to_invoice_label: this.model.total_ht_to_invoice_label(),
            tva_to_invoice_label: this.model.tva_to_invoice_label(),
            ttc_to_invoice_label: this.model.total_ttc_to_invoice_label(),
            show_edit_button: show_edit_button,
            show_percent_widget: this.percentWidgetVisible(),
            not_editable: ! this.model.isUnified(),
        }
        return result
    },
});
export default TaskGroupView;
