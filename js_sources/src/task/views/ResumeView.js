import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import { formatAmount } from '../../math';

var template = require("./templates/ResumeView.mustache");

const ResumeView = Mn.View.extend({
    /*
        Affichage des totaux, attend un TotalModel en paramètre
    */
    template: template,
    modelEvents: {
        'change': 'render'
    },
    templateContext: function(){
        let config = Radio.channel('config');
        let compute_mode = config.request('get:options', 'compute_mode');
        let is_ttc_mode = (compute_mode == 'ttc');
        let has_discounts = this.model.get('ht_before_discounts') != this.model.get('ht');
        let has_insurance = Boolean(this.model.get('insurance'));
        
        return {
            is_ttc_mode: is_ttc_mode,
            ttc: formatAmount(this.model.get('ttc'), true),
            ht: formatAmount(this.model.get('ht'), true),
            ht_before: formatAmount(this.model.get('ht_before_discounts'), true),
            ttc_before: formatAmount(this.model.get('ttc_before_discounts'), true),
            tvas: this.model.tva_labels(),
            has_discounts: has_discounts,
            has_insurance: has_insurance,
            insurance: formatAmount(this.model.get('insurance'), true),
        }
    },
});
export default ResumeView;