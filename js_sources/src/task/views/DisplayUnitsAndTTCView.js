import Mn from 'backbone.marionette';
import DisplayUnitsView from "./lines/DisplayUnitsView";
import DisplayTTCView from "./lines/DisplayTTCView";
import Radio from "backbone.radio";

const DisplayUnitsAndTTCView = Mn.View.extend({
    /**
     * Display DisplayUnitsView and DisplayTTCView
     */

    template: require('./templates/DisplayUnitsAndTTCView.mustache'),
    tagName: 'div',
    className: 'form-section',
    regions: {
        display_units_container: '.display-units-container',
        display_ttc_container: '.display-ttc-container',
    },
    onRender: function(){
        let config = Radio.channel('config');

        let view;
        if(config.request('has:form_section', 'composition:display_units')) {
            view = new DisplayUnitsView({model: this.model});
            this.showChildView('display_units_container', view);
        }

        if (config.request('has:form_section', 'composition:display_ttc')) {
            let business_type = config.request('get:options', 'business_types')[0];
            if (!business_type.tva_on_margin){
                view = new DisplayTTCView({model: this.model})
                this.showChildView('display_ttc_container', view);
            }
        }
    }
});

export default DisplayUnitsAndTTCView;