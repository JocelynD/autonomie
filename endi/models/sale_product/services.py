import logging
from endi_base.models.base import DBSESSION

from endi.compute.math_utils import compute_tva


logger = logging.getLogger(__name__)


class SaleProductService:
    @classmethod
    def is_locked(cls, sale_product):
        from endi.models.sale_product.work_item import WorkItem
        from endi.models.price_study.product import PriceStudyProduct
        from endi.models.price_study.work_item import PriceStudyWorkItem

        if (
            DBSESSION()
            .query(WorkItem.id)
            .filter_by(base_sale_product_id=sale_product.id)
            .count()
            > 0
        ):
            return True
        if (
            DBSESSION()
            .query(PriceStudyProduct.id)
            .filter_by(base_sale_product_id=sale_product.id)
            .count()
            > 0
        ):
            return True
        if (
            DBSESSION()
            .query(PriceStudyWorkItem.id)
            .filter_by(base_sale_product_id=sale_product.id)
            .count()
            > 0
        ):
            return True
        return False

    @classmethod
    def _ensure_tva(cls, product):
        """
        Ensure cohesion between tva and product configuration

        Necessary because we can edit one and not the other leading to undesired
        states
        """
        # We ensure tva/product integrity
        if product.tva_id is None:
            product.product_id = None
        elif product.product is not None and product.product.tva_id != product.tva_id:
            product.product_id = None

    @classmethod
    def _get_computer(cls, sale_product):
        if sale_product.mode == "ht":
            from endi.compute.sale_product.ht_mode import (
                SaleProductHtComputer as Computer,
            )
        elif sale_product.mode == "ttc":
            from endi.compute.sale_product.ttc_mode import (
                SaleProductTtcComputer as Computer,
            )
        else:
            # mode = supplier_ht
            from endi.compute.sale_product.supplier_ht_mode import (
                SaleProductSupplierHtComputer as Computer,
            )
        return Computer(sale_product)

    @classmethod
    def get_ht(cls, sale_product):
        computer = cls._get_computer(sale_product)
        return computer.unit_ht()

    @classmethod
    def get_ttc(cls, sale_product):
        computer = cls._get_computer(sale_product)
        return computer.unit_ttc()

    @classmethod
    def sync_amounts(cls, sale_product):
        sale_product.ht = cls.get_ht(sale_product)
        sale_product.ttc = cls.get_ttc(sale_product)
        DBSESSION().merge(sale_product)

    @classmethod
    def sync_price_study(cls, sale_product):
        """
        Tell all the price study items associated to the given sale_product that
        changes have been made

        :param obj sale_product: The current product
        """
        from endi.models.price_study.product import PriceStudyProduct

        products = PriceStudyProduct.query().filter_by(
            base_sale_product_id=sale_product.id
        )
        # Ici on utilise une boucle car uptodate est un attr de la classe
        # parente l'update direct sur la query ne marchera pas
        for product in products:
            product.uptodate = False
            DBSESSION().merge(product)

        from endi.models.price_study.work_item import PriceStudyWorkItem

        work_items = PriceStudyWorkItem.query().filter_by(
            base_sale_product_id=sale_product.id
        )
        work_items.update({"uptodate": False})

    @classmethod
    def on_before_commit(cls, sale_product, state, changes=None):
        """
        Launched when the product has been added/modified/deleted

        :param obj sale_product: The current product
        :param str state: add/update/delete
        :param dict changes: The attributes that were changed
        """
        need_sync = False
        if state == "update":
            if changes:
                for i in (
                    "supplier_ht",
                    "ht",
                    "ttc",
                    "mode",
                    "margin_rate",
                    "general_overhead",
                    "tva_id",
                ):
                    if i in changes:
                        need_sync = True
                        break
            else:
                need_sync = True

            if "tva_id" in changes:
                cls._ensure_tva(sale_product)

            # We indicate to the associated price study elements that the source
            # object has been changed
            cls.sync_price_study(sale_product)
        elif state == "add":
            need_sync = True

        # We sync amounts
        if need_sync:
            cls.sync_amounts(sale_product)


class WorkItemService:
    @classmethod
    def flat_cost(cls, work_item, unitary=False):
        """
        Collect the flat cost for this work item

        :param bool unitary: Unitary cost ?

        :rtype: int
        """
        result = work_item.supplier_ht or 0

        if not unitary:
            quantity = work_item.quantity or 1
        else:
            quantity = 1
        return quantity * result

    @classmethod
    def cost_price(cls, work_item, unitary=False):
        """
        Compute the cost price of the given sale_product work item

        Use the SaleProductWork's general_overhead if set

        :param bool unitary: Unitary cost ?
        :returns: The result in 10*5 format

        :rtype: int
        """
        # NB : general_overhead est une hybrid_property qui renvoie vers
        # d'autres modèles (produit ou produit composé) si besoin
        overhead = work_item.general_overhead
        if not overhead:
            overhead = 0

        supplier_ht = work_item.flat_cost(unitary)
        result = supplier_ht * (1 + overhead)
        return result

    @classmethod
    def intermediate_price(cls, work_item, unitary=False):
        """
        Compute the intermediate price of a work item

        3/    Prix intermédiaire = Prix de revient / ( 1 - ( Coefficients marge
        + aléas + risques ) )

        :param bool unitary: Unitary cost ?
        """
        # NB : margin_rate est une hybrid_property qui renvoie vers
        # d'autres modèles (produit ou produit composé) si besoin
        margin_rate = work_item.margin_rate
        if not margin_rate:
            margin_rate = 0

        if margin_rate != 1:
            result = cls.cost_price(work_item, unitary)
            result = result / (1 - margin_rate)
        else:
            result = 0

        return result

    @classmethod
    def unit_ht(cls, work_item, contribution=None):
        """
        Compute the ht value for the given work item
        """
        from endi.models.company import Company

        if contribution is None:
            company_id = work_item.base_sale_product.company_id
            contribution = Company.get_contribution(company_id)

        intermediate_price = cls.intermediate_price(work_item, unitary=True)
        if intermediate_price != 0:
            result = intermediate_price
            if isinstance(contribution, (int, float)):
                ratio = 1 - contribution / 100.0
                if ratio != 0:
                    result = intermediate_price / ratio
        else:
            result = work_item.ht or 0
        return result

    @classmethod
    def compute_total_ht(cls, work_item, contribution=None):
        """
        Compute the total ht for the given work_item
        """
        from endi.models.company import Company

        if contribution is None:
            company_id = work_item.base_sale_product.company_id
            contribution = Company.get_contribution(company_id)

        ht = cls.unit_ht(work_item, contribution)
        quantity = work_item.quantity or 1
        return ht * quantity

    @classmethod
    def total_ttc(cls, work_item, contribution=None):
        """
        compute the total ttc for the given work item
        """
        ht = work_item.total_ht
        tva = work_item.tva
        if tva is not None:
            return ht + compute_tva(ht, tva.value)
        else:
            return ht

    @classmethod
    def sync_amounts(cls, work_item, work=None):
        """
        Sync the current work item amounts

        :param obj work_item: The current work_item
        :param obj work: Optionnal work which called this sync_amounts func
        """
        logger.debug("Updating WorkItem unit_ht")
        # Ici on set le _ht et pas ht, voir la classe WorkItem pour mieux
        # comprendre
        work_item._ht = work_item.unit_ht()
        logger.debug("new_value %s" % work_item._ht)

        logger.debug("Updating WorkItem.total_ht")
        work_item.total_ht = work_item.compute_total_ht()
        DBSESSION().merge(work_item)

        if work is None and work_item.sale_product_work is not None:
            work_item.sale_product_work.sync_amounts(work_only=True)
            logger.debug("SaleProductWork ht : %s" % work_item.sale_product_work.ht)
            DBSESSION().merge(work_item.sale_product_work)

    @classmethod
    def sync_price_study(cls, work_item):
        """
        Tell all the price study items associated to the given worm_item that
        changes have been made

        :param obj work_item: The current work_item
        """
        from endi.models.price_study.work_item import PriceStudyWorkItem

        work_items = PriceStudyWorkItem.query().filter_by(work_item_id=work_item.id)

        work_items.update({"uptodate": False})

    @classmethod
    def on_before_commit(cls, work_item, state, changes=None):
        """
        Launched when the product has been flushed yet
        """
        # We ensure tva/product integrity
        if work_item.product_id_editable:
            if work_item.tva_id is None:
                work_item.product_id = None
            elif (
                work_item.product is not None
                and work_item.product.tva_id != work_item.tva_id
            ):
                work_item.product_id = None

        if state == "delete":
            parent = work_item.sale_product_work
            if work_item in parent.items:
                parent.items.remove(work_item)
            parent.sync_amounts()
        else:
            # We sync amounts
            cls.sync_amounts(work_item)

        if state == "update":
            cls.sync_price_study(work_item)


class SaleProductWorkService(SaleProductService):
    @classmethod
    def flat_cost(cls, sale_product):
        """
        Compute the flat cost of a complex sale_product

        1/    Déboursé sec = Total matériaux + Total main d'oeuvre + Total
        matériel affecté
        """
        return sum([item.flat_cost() for item in sale_product.items])

    @classmethod
    def cost_price(cls, sale_product):
        """
        Compute the cost price of the given sale_product work suming the cost of
        its differect items

        If globally specified, uses the sale_product's general overhead
        for the computation

        Prix de revient = Déboursé sec * ( 1 + Coefficient frais généraux )
        """
        return sum([item.cost_price(unitary=False) for item in sale_product.items])

    @classmethod
    def intermediate_price(cls, sale_product):
        """
        Compute the intermediate price

        If globally specified, uses the sale_product's margin rate for the
        computation
        3/    Prix intermédiaire = Prix de revient / ( 1 - ( Coefficients marge
        + aléas + risques ) )
        """
        return sum(
            [item.intermediate_price(unitary=False) for item in sale_product.items]
        )

    @classmethod
    def get_ht(cls, sale_product, contribution=None):
        """
        Compute the unit HT amount for this sale_product
        """
        # Coût d'une unité de produit composé
        return sum(
            [item.total_ht for item in sale_product.items if item.total_ht is not None]
        )

    @classmethod
    def get_ttc(cls, sale_product):
        """
        Compute the TTC amount for the given sale_product
        """
        return sum([item.total_ttc() for item in sale_product.items])

    @classmethod
    def sync_amounts(cls, work, work_only=False):
        """
        :param work_only: Only sync work's amounts else also items'
        """
        if not work_only:
            for item in work.items:
                item.sync_amounts(work)
        work.ht = cls.get_ht(work)
        work.ttc = cls.get_ttc(work)
        DBSESSION().merge(work)

    @classmethod
    def sync_price_study(cls, work):
        """
        Tell all the price study items associated to the given work that
        changes have been made

        :param obj work_item: The current work_item
        """
        from endi.models.price_study.work import PriceStudyWork

        works = PriceStudyWork.query().filter_by(sale_product_work_id=work.id)

        # Ici on utilise une boucle car uptodate est un attr de la classe
        # parente l'update direct sur la query ne marchera pas
        for work in works:
            work.uptodate = False
            DBSESSION().merge(work)

    @classmethod
    def on_before_commit(cls, work, state, changes=None):
        """
        On before commit we update the ht amount

        :param obj work: The current work
        :param str state: add/update/delete
        :param dict changes: The modified attributes
        """
        need_sync = False
        if state == "update":
            if changes:
                for key in ("general_overhead", "margin_rate", "items", "tva_id"):
                    if key in changes:
                        need_sync = True
            else:
                need_sync = True

            if "tva_id" in changes:
                cls._ensure_tva(work)

            cls.sync_price_study(work)

        if need_sync:
            cls.sync_amounts(work)
