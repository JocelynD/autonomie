from endi.views.third_party.customer.rest_api import CustomerRestView


def test_get_customers(
    dbsession,
    get_csrf_request_with_db,
    customer,
    company,
):
    request = get_csrf_request_with_db()
    request.context = company
    view = CustomerRestView(request)
    result = view.collection_get()
    assert len(result) == 1
    assert result[0].name == customer.name


def test_get_customers_other_company(
    dbsession,
    get_csrf_request_with_db,
    customer,
    company2,
):
    request = get_csrf_request_with_db()
    request.context = company2
    view = CustomerRestView(request)
    result = view.collection_get()
    assert len(result) == 0
