from endi.compute.math_utils import compute_ht_from_ttc


class SaleProductTtcComputer:
    """
    Computer used in ttc mode
    """

    def __init__(self, sale_product):
        self.sale_product = sale_product

    def unit_ht(self):
        ttc = self.sale_product.ttc
        tva_object = self.sale_product.tva
        if tva_object is not None:
            return compute_ht_from_ttc(
                ttc,
                tva_object.value,
                float_format=False,
            )
        else:
            return ttc

    def unit_ttc(self):
        """
        Compute the ttc value for the given sale product
        """
        return self.sale_product.ttc
