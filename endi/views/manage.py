"""
    Manage view :
        - last documents page
"""
import logging

from endi.resources import dashboard_resources

log = logging.getLogger(__name__)


def manage(request):
    """
    The manage view
    """
    dashboard_resources.need()
    return dict(title="Mon tableau de bord")


def includeme(config):
    config.add_route("manage", "/manage")
    config.add_view(
        manage,
        route_name="manage",
        renderer="manage.mako",
        permission="manage",
    )
