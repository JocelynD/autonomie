from endi.forms.project import APIProjectListSchema
from endi.models.project import Project
from endi.views import (
    RestListMixinClass,
    BaseRestView,
)
from endi.views.project.lists import ProjectListTools
from .routes import (
    PROJECT_COMPANY_CUSTOMERS_COLLECTION_API,
    PROJECT_COLLECTION_API,
)
from ...models.third_party import Customer


class ProjectRestView(ProjectListTools, RestListMixinClass, BaseRestView):
    """
    Projects REST view, scoped to company

       GET : return list of projects (company_id should be provided as context)
    """

    list_schema = APIProjectListSchema()

    def filter_search(self, query, appstruct):
        search = appstruct["search"]
        if search:
            query = query.filter(
                Project.name.like("%" + search + "%"),
            )
        return query

    def filter_customer_id(self, query, appstruct):
        customer_id = appstruct.get("customer_id")
        if customer_id:
            query = query.filter(Project.customers.any(Customer.id == customer_id))
        return query


def includeme(config):
    config.add_rest_service(
        factory=ProjectRestView,
        route_name=PROJECT_COLLECTION_API,
        collection_route_name=PROJECT_COMPANY_CUSTOMERS_COLLECTION_API,
        view_rights="view_project",
        edit_rights="edit_project",
        add_rights="add_project",
        delete_rights="delete_project",
        collection_view_rights="list_projects",
    )
