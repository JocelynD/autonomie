<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="format_mail" />
<%namespace file="/base/utils.mako" import="format_phone" />
<%namespace file="/base/utils.mako" import="format_address" />
<%namespace file="/base/utils.mako" import="company_list_badges" />
<%namespace file="/base/utils.mako" import="login_disabled_msg" />

<%block name='actionmenucontent'>
<div class='layout flex main_actions'>
% for button in actions:
    ${request.layout_manager.render_panel(button.panel_name, context=button)}
% endfor
</div>
</%block>

<%block name='content'>
<div class='data_display separate_bottom'>
	<h2>
	Informations générales
	% if not company.active:
		<small><span class="icon status caution"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#lock"></use></svg></span>${company_list_badges(company)}</small>
	% endif
	</h2>
	<div class="layout flex two_cols">
		<div class="layout flex">
		    <span class='user_avatar'><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#building"></use></svg></span>
		    <div>
				<h3><a href="${request.route_path(r'/companies/{id}', id=company.id)}">Enseigne ${company.name}</a></h3>
					<p>${company.goal}</p>
					%if company.logo_id:
						<img src="${api.img_url(company.logo_file)}" alt=""  width="250px" />
					%endif
					<dl>
						% if company.email:
							<dt>E-mail</dt>
							<dd>${format_mail(company.email)}</dd>
						% endif
						% for label, attr in (('Téléphone', 'phone'), ('Téléphone portable', 'mobile'),):
							%if getattr(company, attr):
								<dt>${label}</dt>
								<dd>
									% if attr == 'mobile':
										${format_phone(getattr(company, attr), 'mobile')}
									% else:
										${format_phone(getattr(company, attr), 'desk')}
									%endif
								</dd>
							% endif
						% endfor
						% if company.address or company.country or company.city:
							<dt>Adresse</dt>
							<dd>${format_address(company, multiline=False)}</dd>
						% endif
						% if company.activities:
							<dt>Domaine(s) d'activité</dt>
							<dd>
								<ul>
									% for activity in company.activities:
										<li>${activity.label}</li>
									% endfor
								</ul>
							</dd>
						% endif
						% if request.has_permission('admin_treasury'):
							<dt>Code comptable</dt>
							<dd>${company.code_compta or "Non renseigné"}</dd>
							<dt>Compte client général</dt>
							<dd>${company.general_customer_account or "Non renseigné"}</dd>
							<dt>Compte client tiers</dt>
							<dd>${company.third_party_customer_account or "Non renseigné"}</dd>
							<dt>Compte client général interne</dt>
							<dd>${company.internalgeneral_customer_account or "Non renseigné"}</dd>
							<dt>Compte client tiers interne</dt>
							<dd>${company.internalthird_party_customer_account or "Non renseigné"}</dd>
							<dt>Compte fournisseur général</dt>
							<dd>${company.general_supplier_account or "Non renseigné"}</dd>
							<dt>Compte fournisseur tiers</dt>
							<dd>${company.third_party_supplier_account or "Non renseigné"}</dd>

							<dt>Compte fournisseur général interne</dt>
							<dd>${company.internalgeneral_supplier_account or "Non renseigné"}</dd>
							<dt>Compte fournisseur tiers interne</dt>
							<dd>${company.internalthird_party_supplier_account or "Non renseigné"}</dd>
                            <dt>Compte général (classe 4) pour les dépenses</dt>
                            <dd>${company.general_expense_account or "Non renseigné"}</dd>

							<dt>Compte de banque</dt>
							<dd>${company.bank_account or "Non renseigné"}</dd>
							% if enabled_modules['contribution']:
							<dt>Contribution à la CAE</dt>
							<dd>
								<% value = company.get_rate(company.id, 'contribution') %>
								% if value:
									${api.format_float(value)} %
									% if company.contribution is None:
										(par défaut)
									% endif
								% else:
									Non renseigné
								% endif
							</dd>
							% endif
							% if enabled_modules['internalcontribution']:
							<dt>Contribution à la CAE pour la facturation interne</dt>
							<dd>
							<% value = company.get_rate(company.id, 'contribution', 'internal') %>
								% if value:
									${api.format_float(value)} %
									% if company.internalcontribution is None:
										(par défaut)
									% endif
								% else:
									Non renseigné
								% endif
							</dd>
							% endif
							% if enabled_modules['insurance']:
							<dt>Taux d'assurance professionnelle</dt>
							<dd>
								<% value = company.get_rate(company.id, 'insurance') %>
								% if value:
									${api.format_float(value)} %
									% if company.insurance is None:
										(par défaut)
									% endif
								% else:
									Non renseigné
								% endif
							</dd>
							% endif
							% if enabled_modules['internalinsurance']:
							<dt>Taux d'assurance professionnelle pour la facturation interne</dt>
							<dd>
								<% value = company.get_rate(company.id, 'insurance', 'internal') %>
								% if value:
									${api.format_float(value)} %
									% if company.internalinsurance is None:
										(par défaut)
									% endif
								% else:
									Non renseigné
								% endif
							</dd>
							% endif
							<dt>RIB / IBAN</dt>
							<dd>${company.RIB or 'Non renseigné'} / ${company.IBAN or 'Non renseigné'}</dd>
						% endif
						<dt>Coefficient de frais généraux</dt>
						<dd>${company.general_overhead or 0}</dd>
						<dt>Coefficient de marge</dt>
						<dd>${company.margin_rate or 0}</dd>
						<dt>CGV complémentaires</dt>
						<dd>${company.cgv and 'Renseignées' or "Non renseignées"}</dd>
						<dt>En-tête des documents</dt>
						<dd>${company.header_id and 'Personalisé (image)' or "Par défaut"}</dd>
					</dl>
			</div>
		</div>
		<div>
			<ul class="no_bullets">
                % for perm, route, label in ( \
                ('list.estimation', 'company_estimations', "Voir les devis",),\
                ('list.invoice', 'company_invoices', "Voir les factures"),\
                ):
                    % if request.has_permission(perm):
                    <li>
                        <span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#arrow-right"></use></svg></span>
                        <a href="${request.route_path(route, id=_context.id)}">${label}</a>
                    </li>
                    % endif
                % endfor
                % for module, perm, route, label in ( \
                ('commercial', 'view.commercial', 'commercial_handling', "Voir la gestion commerciale"), \
                ('accounting', 'view.treasury', '/companies/{id}/accounting/treasury_measure_grids', "Voir les états de trésorerie"), \
                ('accompagnement', 'list.activity', 'company_activities', "Voir les rendez-vous"),\
                ('workshops', 'list.workshop', 'company_workshops_subscribed', "Voir les ateliers auxquels l’enseigne participe"),\
                ('training', 'list.training', 'company_workshops', "Voir les ateliers organisés par l’enseigne"),\
                ):
                    % if request.has_module(module) and request.has_permission(perm):
                    <li>
                        <span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#arrow-right"></use></svg></span>
                        <a href="${request.route_path(route, id=_context.id)}">${label}</a>
                    </li>
                    % endif
                % endfor
			</ul>
		</div>
	</div>
</div>
<div class="data_display">
	<h2>Employé(s)</h2>
	<div class='panel-body'>
		<ul class="company_employees">
		% for user in company.employees:
			<li class="company_employee_item">
				% if user.photo_file:
					<span class="user_avatar">
						<img src="${api.img_url(user.photo_file)}"
							title="${api.format_account(user)}"
							alt="Photo de ${api.format_account(user)}"
							width="256" height="256" />
					</span>
				% else:
					<span class="user_avatar"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#user"></use></svg></span>
				% endif
				% if request.has_permission("view.user", user):
					<a href="${request.route_path('/users/{id}', id=user.id)}" title='Voir ce compte'>
				% endif
				${api.format_account(user)}
				% if request.has_permission("view.user", user):
					</a>
				% endif
				% if user.login is not None and not user.login.active:
				    <small>${login_disabled_msg()}</small>
				% endif
			</li>
		% endfor
		</ul>
		% if len(company.employees) == 0:
			Aucun entrepreneur n’est associé à cette enseigne
		% endif
	</div>
</div>
</%block>
