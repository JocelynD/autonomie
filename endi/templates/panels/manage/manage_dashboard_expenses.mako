<!-- DEPENSES EN ATTENTE DE VALIDATION -->
<div class="dash_elem">
    <h2>
        <span class='icon'><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#credit-card"></use></svg></span>
        <a href="/expenses" title="Voir toutes les Notes de dépense">
            Notes de dépense en attente de validation
            <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#arrow-right"></use></svg>
        </a>
    </h2>
    <div class='panel-body'>
        <table class="hover_table">
            <caption class="screen-reader-text">Liste des notes de dépenses en attente de validation</caption>
            <thead>
                <tr>
                    <th scope="col" class="col_text">Période</th>
                    <th scope="col" class="col_text">Entrepreneur</th>
                    <th scope="col" class="col_date">Demandé le</th>
                </tr>
            </thead>
            <tbody>
                % for expense in expenses:
                    <tr class="clickable-row" data-href="${expense.url}" title="Voir la note de dépenses&nbsp;: ${api.month_name(expense.month)} ${expense.year} pour ${api.format_account(expense.user)}">
                        <td class="col_text"><a href="${expense.url}">${api.month_name(expense.month)} ${expense.year}</a></td>
                        <td class="col_text">${api.format_account(expense.user)}</td>
                        <td class="col_date">${api.format_date(expense.status_date)}</td>
                    </tr>
                % endfor
                % if not expenses:
                    <tr><td class="col_text" colspan='3'><em>Aucune note de dépenses en attente</em></td></tr>
                % endif
            </tbody>
        </table>
    </div>
</div>

